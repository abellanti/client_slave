#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>

#include <ESP8266WiFiMulti.h>
ESP8266WiFiMulti WiFiMulti;

const int redButton=16;
const int yellowButton=5;
const int greenButton=4;
const char* ssid = "5G-Bill-Gates-Mental-Control";
const char* password = "123456789";
const char* serverAddress = "http://192.168.4.1/";
String color="";

void wifiConnect(){
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("Connected to WiFi");
}

String httpGETRequest(const char* serverName) {
  WiFiClient client;
  HTTPClient http;
    
  // Your IP address with path or Domain name with URL path 
  http.begin(client, serverName);
  
  // Send HTTP POST request
  int httpResponseCode = http.GET();
  
  String payload = "none"; 
  
  if (httpResponseCode>0) {
    Serial.print("HTTP Response code: ");
    Serial.println(httpResponseCode);
    payload = http.getString();
  }
  else {
    Serial.print("Error code: ");
    Serial.println(httpResponseCode);
  }
  // Free resources
  http.end();

  return payload;
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(redButton, OUTPUT);
  pinMode(yellowButton, OUTPUT);
  pinMode(greenButton, OUTPUT);
  wifiConnect();
  
}

void loop() {
  // put your main code here, to run repeatedly:
  if ((WiFiMulti.run() == WL_CONNECTED)) {
    String color  = httpGETRequest(serverAddress);
    if(color.equals("red")){
      digitalWrite(greenButton,LOW);
      digitalWrite(yellowButton,LOW);
      digitalWrite(redButton,HIGH);
    }
    else if(color.equals("yellow")){
      digitalWrite(greenButton,LOW);
      digitalWrite(yellowButton,HIGH);
      digitalWrite(redButton,LOW);
    }
    else if(color.equals("green")){
      digitalWrite(greenButton,HIGH);
      digitalWrite(yellowButton,LOW);
      digitalWrite(redButton,LOW);
    }
    else{
      digitalWrite(greenButton,LOW);
      digitalWrite(redButton,HIGH);
      delay(250);
      digitalWrite(redButton,LOW);
      digitalWrite(yellowButton,HIGH);
      delay(250);
      digitalWrite(yellowButton,LOW);
      digitalWrite(greenButton,HIGH);
      delay(250);
    }
  }
  else{
    wifiConnect();
  }
}
